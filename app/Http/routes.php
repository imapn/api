  <?php

  $app->register('Dingo\Api\Provider\LumenServiceProvider');

  $api = $app['api.router'];
  /*
   * Version 1 API
   */
  $api->version('v1', function ($api) use ($app) {

    $api->group(['namespace' => 'App\Http\Controllers'], function ($api) {
        $api->get('/', ['uses' => 'BaseController@helloWorld']);

        $api->group(['prefix' => 'auth'], function ($api) {
            $api->post('/facebook', ['uses' => 'AuthController@facebook']);
            $api->post('/login', ['uses' => 'AuthController@login']);
            $api->post('/login/admin', ['uses' => 'AuthController@loginAdmin']);
            $api->post('/register', ['uses' => 'AuthController@register']);
        });

        $api->group(['prefix' => 'checkin'], function ($api) {
            $api->post('/', ['uses' => 'CheckInsController@insert']);
        });

        $api->group(['prefix' => 'feedbacks'], function ($api) {
            $api->post('/', ['uses' => 'FeedbacksController@insert']);
            $api->get('/', ['uses' => 'FeedbacksController@list']);
        });

        $api->group(['prefix' => 'items'], function ($api) {
            $api->post('/', ['uses' => 'ItemsController@insert']);
            $api->get('/', ['uses' => 'ItemsController@list']);
        });

        $api->group(['prefix' => 'social'], function ($api) {
            $api->post('/share', ['uses' => 'SocialController@sharePhoto']);
        });

        $api->group(['prefix' => 'places'], function ($api) {
            $api->post('/', ['uses' => 'PlacesController@insert']);

            $api->group(['prefix' => 'favorite'], function ($api) {
                $api->post('/', ['uses' => 'PlacesController@favorite']);
                $api->delete('/{delete_id}', ['uses' => 'PlacesController@favoriteDelete']);
            });
            
            $api->get('/', ['uses' => 'PlacesController@list']);
            $api->get('/user/{user_id}', ['uses' => 'PlacesController@listByUser']);
            $api->get('/user/{user_id}/{lat}/{lgn}/{distance}/{limit}', ['uses' => 'PlacesController@listByUserByDistance']);

            $api->group(['prefix' => '{place_id}'], function ($api) {
                $api->get('/', ['uses' => 'PlacesController@get']);
                $api->put('/', ['uses' => 'PlacesController@update']);
                $api->group(['prefix' => 'user'], function ($api) {
                        $api->get('/{user_id}', ['uses' => 'PlacesController@getByUser']);
                });
            });
        });

        $api->group(['prefix' => 'user'], function ($api) {
            $api->get('/', ['uses' => 'AppController@list']);

            $api->group(['prefix' => '{user_id}'], function ($api) {
                $api->get('/', ['uses' => 'AppController@getUser']);
                $api->post('/item', ['uses' => 'UserItemsController@insert']);
            });
        });

    });
  });
