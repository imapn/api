<?php

namespace App\Http\Controllers;

use Dingo\Api\Routing\Helpers;
use Illuminate\Routing\Controller;
use App\Http\Controllers\AppController;
use Illuminate\Http\Request;
// MODELS
use App\Models\Users;

class AuthController extends BaseController
{
    function __construct()
    {
      $this->Users = new Users;
    }
    /**
    * @api {post} /auth/facebook Facebook
    * @apiGroup Auth
    * @apiName Facebook
    * @apiParamExample {json} Request-Example:
    *     {
    *       "facebook_id": "STRING",
    *       "facebook_token": "STRING"
    *     }
    * @apiSuccessExample {json} 200 OK
    * {
    *   "User":
    *     {
    *       "_id": "57ae75bda697b2001046b09012390",
    *       "facebook_id": "57ae754da697b2000c0ba171",
    *       "facebook_token": "57ae754da697b2000c0ba171012909129010911",
    *       "updated_at": "2016-08-13 01:19:57",
    *       "created_at": "2016-08-13 01:19:57"
    *     }
    * }
    * @apiErrorExample {json} 422 Invalid
    * {
    *   "error": {
    *     "message": "Usuário não pode ser criado",
    *     "errors": [
    *       [
    *         "facebook id não informado"
    *       ]
    *     ],
    *     "status_code": 422
    *   }
    * }
    */
    function facebook(Request $request)
    {
      return $this->response->array( $this->Users->facebook( $request->input() ) );
    }

    function login(Request $request)
    {
      return $this->response->array( $this->Users->login( $request->input() ) );
    }

    function loginAdmin(Request $request)
    {
      return $this->response->array( $this->Users->loginAdmin( $request->input() ) );
    }

    function register(Request $request)
    {
      return $this->response->array( $this->Users->register( $request->input() ) );
    }

}
