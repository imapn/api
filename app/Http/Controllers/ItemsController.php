<?php

namespace App\Http\Controllers;

use Dingo\Api\Routing\Helpers;
use Illuminate\Routing\Controller;
use App\Models\Items;
use App\Http\Controllers\AppController;
use Illuminate\Http\Request;

class ItemsController extends BaseController
{
    function __construct()
    {
      $this->Items = new Items;
      $this->AppController = new AppController;
    }

    function insert( Request $request )
    {
      return $this->response->array( $this->Items->insert( $request->input() )  );
    }

    function getByUser( $_id, $user)
    {
      return $this->response->array( $this->Items->getByUser( $_id, $user )  );
    }

    function list(Request $request)
    {
      return $this->response->array( $this->AppController->query($this->Items, $request)->toArray() );
    }
}
