<?php

namespace App\Http\Controllers;

use Dingo\Api\Routing\Helpers;
use Illuminate\Routing\Controller;
use App\Models\CheckIns;
use App\Http\Controllers\AppController;
use Illuminate\Http\Request;

class CheckInsController extends BaseController
{
    function __construct()
    {
      $this->CheckIns = new CheckIns;
      $this->AppController = new AppController;
    }

    function insert( Request $request )
    {
      return $this->response->array( $this->CheckIns->insert( $request->input() )  );
    }

}
