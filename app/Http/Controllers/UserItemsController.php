<?php

namespace App\Http\Controllers;

use Dingo\Api\Routing\Helpers;
use Illuminate\Routing\Controller;
use App\Models\UserItems;
use App\Http\Controllers\AppController;
use Illuminate\Http\Request;

class UserItemsController extends BaseController
{
    function __construct()
    {
      $this->UserItems = new UserItems;
      $this->AppController = new AppController;
    }

    function insert( Request $request, $User_id )
    {
      return $this->response->array( $this->UserItems->insert( array_merge($request->input(), ['user_id' => $User_id]) )  );
    }

    function getByUser( $_id, $user)
    {
      return $this->response->array( $this->UserItems->getByUser( $_id, $user )  );
    }

    function list(Request $request)
    {
      return $this->response->array( $this->AppController->query($this->UserItems, $request)->toArray() );
    }
}
