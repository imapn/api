<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Validator;
use App\Models\ValidatorModel;

class Items extends Eloquent
{
  protected $collection = 'Items';
  protected $ValidatorModel;
  protected static $unguarded = true;

  public function validateInsertItems( array $inputs )
  {
    $validator = Validator::make(
      $inputs,
    [
    //   'item_id' => 'required|string|exists:Items,_id',
      'place_id' => 'required|string|exists:Places,_id',
    //   'user_id' => 'required|string|exists:Users,_id',
      'name' => 'required|string',
      'text'     => 'required|string',
      'pointing'     => 'required|string',
    ], ValidatorModel::$validatorMessage );
    if ($validator->fails()) throw new \Dingo\Api\Exception\StoreResourceFailedException('Parametro incorreto', array_values(array_filter($validator->errors()->toArray())));
    return false;
  }

  public function insert( array $inputs )
  {
    $this->validateInsertItems( $inputs );
    return $this::create( $inputs );
  }
}
