<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Validator;
use App\Models\ValidatorModel;
use anlutro\cURL\cURL;

use App\Models\CheckIns;
use App\Models\Feedbacks;
use App\Models\Users;
use App\Models\Items;
use App\Models\PlacesFavorites;
use App\Models\UserItems;

class Places extends Eloquent
{
  // protected $cURL;
  protected $collection = 'Places';
  protected $ValidatorModel;
  protected static $unguarded = true;

  public function validateCreatePlaces( array $inputs )
  {
    $validator = Validator::make(
      $inputs,
    [
      'name' => 'required|string',
      'address' => 'required|string',
      'code' => 'required|string',
      'state' => 'string',
      'city' => 'string', 
      'img'     => 'string',
      'itens'     => 'array',
      'location'     => 'array',
      'location.latitude'     => 'string',
      'location.longitude'     => 'string',
    ], ValidatorModel::$validatorMessage );
    if ($validator->fails()) throw new \Dingo\Api\Exception\StoreResourceFailedException('Parametros incorretos', array_values(array_filter($validator->errors()->toArray())));
    return false;
  }

  public function insert( array $inputs )
  {
    // Validate Inputs
    $this->validateCreatePlaces( $inputs );
    $location = $this->getAddress( $inputs['address'] );
    $inputs = array_merge( $inputs, [
      'address' => $location->formatted_address,      
      'location' => [ 
        'latitude' => $location->geometry->location->lat, 'longitude' => $location->geometry->location->lng ]
        ]);    
    $insert = $this::create( array_merge( $inputs, ["itens" => ""]) );
    if(!empty( $inputs['itens'] )) $this->insertItens( $insert->_id , $inputs['itens'] );
    return $insert;
  }

  function insertItens( $place_id, $itens )
  {
    foreach ($itens as $key => $value) 
    {
      (new Items)->insert( array_merge($value, ['place_id' => $place_id]  ) );
    }
    return true;
  }

  function getAddress( $address )
  {
    $address = urlencode($address);
    // Request Google Maps by address 
    $response = (new cURL)->get("http://maps.googleapis.com/maps/api/geocode/json?address=$address");
    // Validate address
    if( empty( json_decode($response->body)->results) ) throw new \Dingo\Api\Exception\StoreResourceFailedException('Endereço  incorreto', [] );    
    return json_decode($response->body)->results['0'];
  }
  public function get( string $_id )
  {
    $this->validateData( [ '_id' => $_id ]);
    return $this::where('_id', $_id)->first()->toArray();
  }

public static function getNearby($lat, $lng, $distance = 1, $limit = 5, $unit = 'km')
{
    // radius of earth; @note: the earth is not perfectly spherical, but this is considered the 'mean radius'
    if ($unit == 'km') $radius = 6371.009; // in kilometers
    elseif ($unit == 'mi') $radius = 3958.761; // in miles

    // latitude boundaries
    $maxLat = (float) $lat + rad2deg($distance / $radius);
    $minLat = (float) $lat - rad2deg($distance / $radius);

    // longitude boundaries (longitude gets smaller when latitude increases)
    $maxLng = (float) $lng + rad2deg($distance / $radius / cos(deg2rad((float) $lat)));
    $minLng = (float) $lng - rad2deg($distance / $radius / cos(deg2rad((float) $lat)));


    return [
      'minLat' => $minLat,
      'maxLat' => $maxLat,
      'minLng' => $minLng,
      'maxLng' => $maxLng,
      'minLat' => $minLat,
    ];
}


  public function getByUser( string $_id, string $user, $Return = [] )
  {
    $this->validateData( [ '_id' => $_id, 'user' => $user ]);
    $Feedbacks = Feedbacks::select('user_id', 'text', 'name', 'created_at')->where('place_id', $_id )->orderBy('created_at', 'DESC')->get()->toArray();
    $FeedbacksUser = Feedbacks::where('place_id', $_id )->where('user_id', $user )->exists();
    $UsersFeedbacks = Users::whereIn('_id', array_column($Feedbacks, 'user_id') )->get()->keyBy('_id')->toArray();
    $Items = Items::where('place_id', $_id )->orderBy('pointing', 'ASC')->get()->keyBy('_id')->toArray();
    $UserPoints = (new UserItems)->getActivesPointUserByPlace( $_id,  $user);
    if( $UserPoints["actives"] > 10 OR $UserPoints["actives"] == 0 ) UserItems::where('user_id', $user )->where('place_id', $_id )->update(['status' => 'inactive']);
  
    $UserItems = UserItems::where('user_id', $user )->where('place_id', $_id )->where('status', 'active' )->get()->keyBy('item_id')->toArray();

    foreach ($Items  as $key => $value) {
      if( !empty( $UserItems[$value["_id"] ] )) { $Return['Items'][] = array_merge($value, ['get' => true]);}
      else $Return['Items'][] = $value;
    }
    foreach ($Feedbacks as $key => $value) {
      $Return['Feedbacks'][] = array_merge( $value, [ 'User' => $UsersFeedbacks[$value['user_id']] ]);
    }

    return array_merge( $this::where('_id', $_id)->first()->toArray(), 
    [
      'Feedbacks' => (!empty( $Return['Feedbacks'] ) ) ? $Return['Feedbacks'] : [],
      'Items' => (!empty( $Return['Items'] ) ) ? $Return['Items'] : [],
      'UserPoints' =>  $UserPoints,
      'FeedbacksUser' => $FeedbacksUser
    ]);
  }

  public function validateData( array $inputs )
  {
    $validator = Validator::make(
      $inputs,
    [
      '_id' => 'string|exists:Places,_id',
      'user' => 'string|exists:Users,_id',

    ], ValidatorModel::$validatorMessage );
    if ($validator->fails()) throw new \Dingo\Api\Exception\StoreResourceFailedException('Parametros incorrretos', array_values(array_filter($validator->errors()->toArray())));
    return false;
  }

  public function validateDataFavorite( array $inputs )
  {
    $validator = Validator::make(
      $inputs,
    [
      'place_id' => 'required|string|exists:Places,_id',
      'user_id' => 'required|string|exists:Users,_id',

    ], ValidatorModel::$validatorMessage );
    if ($validator->fails()) throw new \Dingo\Api\Exception\StoreResourceFailedException('Parametros incorrretos', array_values(array_filter($validator->errors()->toArray())));
    return false;
  }

  public function listByUser( array $Places, $user, $Return = [])
  {
    $this->validateData( ['user' => $user] );
    $PlacesFavorite = PlacesFavorites::where('user_id' , $user)->get()->keyBy('place_id')->toArray();
    foreach ($Places as $key => $value) 
    {
      if(!empty( $PlacesFavorite[$value["_id"]] )) $Places[$key] = array_merge($Places[$key], ['favorite' => true]);
    }
    return $Places;
  }

 public function listByUserByDistance( $user, $lat, $lng, $distance, $limit, $Return = [])
  {
    $this->validateData( ['user' => $user] );
    $nearby =  $this->getNearby($lat, $lng, $distance, $limit);
    $Places = Places::select('_id', 'code',  'name', 'type', 'points', 'state', 'city', 'img', 'location')->whereBetween('location.latitude', [$nearby['minLat'], $nearby['maxLat']])->whereBetween('location.longitude', [$nearby['minLng'], $nearby['maxLng']])->get()->toArray();
    $PlacesFavorite = PlacesFavorites::where('user_id' , $user)->get()->keyBy('place_id')->toArray();
    foreach ($Places as $key => $value) 
    {
      $Places[$key] = array_merge($Places[$key] ,(new UserItems)->getActivesPointUserByPlace( $value["_id"] ,  $user) );
      if(!empty( $PlacesFavorite[$value["_id"]] )) $Places[$key] = array_merge($Places[$key], ['favorite' => true]);
    }
    return $Places;
  }

  public function favorite( array $inputs )
  {
    $this->validateDataFavorite( $inputs );
    $PlacesFavorite = PlacesFavorites::where('user_id' , $inputs['user_id'])->where('place_id' , $inputs['place_id'])->delete();
    if( $PlacesFavorite ) return ['message' => false];
    PlacesFavorites::create( $inputs );
    return ['message' => true];
  }

  public function edit( $inputs, $_id )
  {
    $this->validateData( [ '_id' => $_id ]);
    if( !empty( $inputs['address'] ))
    {
      $location = $this->getAddress( $inputs['address'] );
      $inputs = array_merge( $inputs, [
        'address' => $location->formatted_address,      
        'location' => [ 
          'latitude' => $location->geometry->location->lat, 'longitude' => $location->geometry->location->lng ]
          ]); 
    }
    $this::where( '_id', $_id )->update( $inputs );
    return ['message' => 'true'];
  }
}
